.. Fuego documentation master file, created by
   sphinx-quickstart on Tue Nov 21 17:18:07 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Fuego's documentation!
=================================

Fuego is a test system specifically designed for embedded Linux
testing. It supports automated testing of embedded targets from
a host system, as it's primary method of test execution.

The quick introduction to Fuego is that it consists of a host/target
script engine and over 100 pre-packaged tests. These are installed
in a docker container along with a Jenkins web interface and
job control system, ready for out-of-the-box Continuous Integration
testing of your embedded Linux project.

The ideas is that in the simplest case, you just add your board,
a toolchain, and go!

This documentation provides instructions (or it will, when it is finished).
(This documentation is a placeholder for the moment.)

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
